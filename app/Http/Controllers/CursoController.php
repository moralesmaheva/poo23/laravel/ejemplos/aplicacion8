<?php

namespace App\Http\Controllers;

use App\Http\Requests\CursoRequest;
use App\Models\Curso;
use Illuminate\Http\Request;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //sacamos los cursos
        $cursos=Curso::all();
        return view('curso.index',compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('curso.create', [
            'curso' => new Curso()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CursoRequest $request)
    {
        //crear el curso
        $curso=Curso::create($request->all());
        //redirigir
        return redirect()
        ->route('curso.show', $curso->id)
        ->with('success','Curso creado con exito');
    }

    /**
     * Display the specified resource.
     */
    public function show(Curso $curso)
    {
        return view('curso.show',compact('curso'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Curso $curso)
    {
        return view('curso.edit',compact('curso'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CursoRequest $request, Curso $curso)
    {
        ///actualizar el curso
        $curso->update($request->all());
        //redirigir
        return redirect()
        ->route('curso.index', $curso)
        ->with('success','Curso actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Curso $curso)
    {
        
        //eliminamos el curso
        $curso->delete();
        //redirigir
        return redirect()
        ->route('curso.index')
        ->with('success','Curso eliminado con exito');
    }
}
