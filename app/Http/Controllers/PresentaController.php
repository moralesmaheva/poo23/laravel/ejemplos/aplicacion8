<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use App\Models\Practica;
use App\Models\Presenta;
use Illuminate\Http\Request;

class PresentaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //listamos los registros
        $presentas=Presenta::all();
        return view('presenta.index',compact('presentas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //creamos los alumnos y las practicas
        $alumnos=Alumno::all();
        $practicas=Practica::all();
        //devolvemos la vista
        return view('presenta.create', compact('alumnos', 'practicas'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //creamos la Presenta
        $presenta=Presenta::create($request->all());
        //redirigimos
        return redirect()
        ->route('presenta.show', $presenta);
    }

    /**
     * Display the specified resource.
     */
    public function show(Presenta $presentum)
    {
        return view('presenta.show', compact('presentum'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Presenta $presentum)
    {
        return view('presenta.edit')
        //enviamos el listado de alumnos
        ->with('alumnos', Alumno::all())
        //enviamos el listado de practicas
        ->with('practicas', Practica::all())
        //enviamos la Presenta
        ->with('presenta', $presentum);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Presenta $presentum)
    {
        //actualizamos la Presenta
        $presentum->update($request->all());
        //redirigimos
        return redirect()
        ->route('presenta.show', $presentum->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Presenta $presentum)
    {
        //eliminamos la Presenta
        $presentum->delete();
        //redirigimos
        return redirect()
        ->route('presenta.index');
    }
}
