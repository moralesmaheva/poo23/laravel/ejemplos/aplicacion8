<div>
    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" id="nombre" value="{{ old('nombre', $alumno->nombre) }}">
    @error('nombre')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <label for="apellidos">Apellidos</label>
    <input type="text" name="apellidos" id="apellidos" value="{{ old('apellidos', $alumno->apellidos) }}">
    @error('apellidos')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <label for="fechanacimiento">Fecha de nacimiento</label>
    <input type="date" name="fechanacimiento" id="fechanacimiento"
        value="{{ old('fechanacimiento', $alumno->fechanacimiento) }}">
    @error('fechanacimiento')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <label for="email">Email</label>
    <input type="email" name="email" id="email" value="{{ old('email', $alumno->email) }}">
    @error('email')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <label for="foto">Foto</label>
    @if ($alumno->foto)
        <img src="{{ asset('storage/' . $alumno->foto) }}" id="preview">
    @else
        <img src="" id="preview">
    @endif
    <input type="file" name="foto" id="foto" value="{{ $alumno->foto }}" id="fichero">
    @error('foto')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <button type="submit" class="boton">Guardar</button>
</div>
