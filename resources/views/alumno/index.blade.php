@extends('layouts.main')

@section('content')
    <h1>Alumnos</h1>
<div class="listado">
    @foreach ($alumnos as $alumno)
        <div class="tarjeta">
            <ul>
                <li>{{ $alumno->id }}</li>
                <li>Nombre: {{ $alumno->nombre }}</li>
                <li>Aplellidos: {{ $alumno->apellidos }}</li>
                <li>Fecha de nacimiento: {{ $alumno->fechanacimiento }}</li>
                <li>Email: {{ $alumno->email }}</li>
                <li>Foto: {{ $alumno->foto }}</li>
            </ul>
            <div class="botones">
                <a href="{{ route('alumno.show', $alumno) }}" class="boton">Ver</a>
                <a href="{{ route('alumno.edit', $alumno) }}" class="boton">Editar</a>
                <form action="{{ route('alumno.destroy', $alumno) }}" method="post" id="eliminar">
                    @csrf
                    @method('delete')
                    <button type="submit" class="boton">Borrar</button>
                </form>
            </div>
        </div>
    @endforeach
</div>
@endsection
