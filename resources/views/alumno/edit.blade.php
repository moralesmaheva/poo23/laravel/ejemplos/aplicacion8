@extends('layouts.main')

@section('content')
<div>
    <h1>Actualizar alumno</h1>
</div>
<div class="tarjeta">
    <form action="{{ route('alumno.update',$alumno) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        @include('alumno._form')
    </form>
</div>
@endsection