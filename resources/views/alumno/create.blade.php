@extends('layouts.main')

@section('content')
<div>
    <h1>Crear nuevo alumno</h1>
</div>
<div class="tarjeta">
    <form action="{{ route('alumno.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        @include('alumno._form')
    </form>
</div>
@endsection
