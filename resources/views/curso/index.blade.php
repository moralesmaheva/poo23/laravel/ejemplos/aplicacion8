@extends('layouts.main')

@section('content')
    <h1>Cursos</h1>
<div class="listado">
    @foreach ($cursos as $curso)
        <div class="tarjeta">
            <ul>
                <li>{{ $curso->id }}</li>
                <li>Nombre: {{ $curso->nombre }}</li>
                <li>Fecha de comienzo: {{ $curso->fechacomienzo }}</li>
                <li>Duracion: {{ $curso->duracion }}</li>
                <li>Observaciones: {{ $curso->observaciones }}</li>
            </ul>
            <div class="botones">
                <a href="{{ route('curso.show', $curso->id) }}" class="boton">Ver</a>
                <a href="{{ route('curso.edit', $curso->id) }}" class="boton">Editar</a>
                <form action="{{ route('curso.destroy', $curso) }}" method="post" id="eliminar" class="form-inline">
                    @csrf
                    @method('delete')
                    <button type="submit" class="boton">Borrar</button>
                </form>
            </div>
        </div>
    @endforeach
</div>
@endsection
