@extends('layouts.main')

@section('content')
    <div>
        <h1>Crear nuevo curso</h1>
    </div>
    <form action="{{ route('curso.store') }}" method="post">
        @csrf
        @include('curso._form')
    </form>
@endsection