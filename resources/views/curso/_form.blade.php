<div>
    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" id="nombre" value="{{ old('nombre', $curso->nombre) }}">
    @error('nombre')
        <div class="error">{{ $message }}></div>
    @enderror
</div>
<div>
    <label for="fechacomienzo">Fecha de comienzo</label>
    <input type="date" name="fechacomienzo" id="fechacomienzo"
        value="{{ old('fechacomienzo', $curso->fechacomienzo) }}">
    @error('fechacomienzo')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <label for="duracion">Duracion</label>
    <input type="number" name="duracion" id="duracion" value="{{ old('duracion', $curso->duracion) }}">
    @error('duracion')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <label for="observaciones">Observaciones</label>
    <input type="text" name="observaciones" id="observaciones"
        value="{{ old('observaciones', $curso->observaciones) }}">
    @error('observaciones')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <button type="submit" class="boton">Guardar</button>
</div>