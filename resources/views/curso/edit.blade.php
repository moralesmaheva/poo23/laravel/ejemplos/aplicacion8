@extends('layouts.main')

@section('content')
    <div>
        <h1>Actualizar curso</h1>
    </div>
    <form action="{{ route('curso.update',$curso) }}" method="post">
        @csrf
        @method('PUT')
        @include('curso._form')
    </form>
@endsection
