@extends('layouts.main')

@section('content')
    <h1>Entregas</h1>
<div class="listado">
    @foreach ($presentas as $presentum)
        <div class="tarjeta">
            <ul>
                <li>{{ $presentum->id }}</li>
                <li>Nota: {{ $presentum->nota }}</li>
                <li>Alumno: {{ $presentum->alumno->nombre }} {{ $presentum->alumno->apellidos }}</li>
                <li>Practica: {{ $presentum->practica->id }}-{{ $presentum->practica->titulo }}</li>
            </ul>
            <div class="botones">
                <a href="{{ route('presenta.show', $presentum) }}" class="boton">Ver</a>
                <a href="{{ route('presenta.edit', $presentum) }}" class="boton">Editar</a>
                <form action="{{ route('presenta.destroy', $presentum) }}" method="post" id="eliminar">
                    @csrf
                    @method('delete')
                    <button type="submit" class="boton">Borrar</button>
                </form>
            </div>
        </div>
    @endforeach
</div>
@endsection
