@extends('layouts.main')

@section('content')
<div>
    <h1>Actualizar entrega</h1>
</div>
    <form action="{{ route('presenta.update',$presenta) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div>
            <label for="nota">Nota</label>
            <input type="number" name="nota" id="nota" value="{{ old('nota', $presenta->nota) }}">
        </div>
        <div>
            <label>Alumno: </label>
            <select name="alumno_id">
                @foreach ($alumnos as $alumno)
                    <option value="{{ $alumno->id }}" 
                        {{ old('alumno_id', $presenta->alumno_id) == $alumno->id ? 'selected' : '' }}>
                        {{ $alumno->id }} - {{ $alumno->nombre }} {{ $alumno->apellidos }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label>Practica: </label>
            <select name="practica_id">
                @foreach ($practicas as $practica)
                    <option value="{{ $practica->id }}" 
                        {{ old('practica_id', $presenta->practica_id) == $practica->id ? 'selected' : '' }}>
                        {{ $practica->id }} - {{ $practica->titulo }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <button type="submit" class="boton">Actualizar</button>
        </div>
    </form>
@endsection