@extends('layouts.main')

@section('content')
<div>
    <h1>Gestion de alumnos y cursos</h1>
</div>
    <form action="{{ route('pertenece.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div>
            <label>Selecciona el Alumno</label>
            <select name="alumno_id">
                @foreach ($alumnos as $alumno)
                    <option value="{{ $alumno->id }}">{{ $alumno->id }} - {{ $alumno->nombre }} {{ $alumno->apellidos }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label>Selecciona el curso</label>
            <select name="curso_id">
                @foreach ($cursos as $curso)
                    <option value="{{ $curso->id }}">{{ $curso->id }} - {{ $curso->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <button type="submit" class="boton">Guardar</button>
        </div>
    </form>
@endsection
