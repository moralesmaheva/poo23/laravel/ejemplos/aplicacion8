<div>
    <label for="titulo">Titulo</label>
    <input type="text" name="titulo" id="titulo" value="{{ old('titulo', $practica->titulo) }}">
    @error('titulo')
        <div class="error">{{ $message }}></div>
    @enderror
</div>
<div>
    <label for="fichero">Fichero</label>
    <div>{{ $practica->fichero }}</div>
    <input type="file" name="fichero" id="fichero" value="{{ $practica->fichero }}">
    @error('fichero')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <label>Selecciona el curso</label>
    <select name="curso_id">
        <option value="">-- Seleccione --</option>
        @foreach ($cursos as $curso)
            <option value="{{ $curso->id }}" 
                {{ old('curso_id', $practica->curso_id) == $curso->id ? 'selected' : '' }}>
                {{ $curso->id }} - {{ $curso->nombre }}</option>
        @endforeach
    </select>
    @error('curso_id')
        <div class="error">{{ $message }}</div>
    @enderror
</div>
<div>
    <button type="submit" class="boton">Guardar</button>
</div>