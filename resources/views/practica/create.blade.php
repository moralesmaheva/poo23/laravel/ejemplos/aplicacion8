@extends('layouts.main')

@section('content')
<div>
    <h1>Crear nueva practica</h1>
</div>
    <form action="{{ route('practica.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        @include('practica._form')
    </form>
@endsection
