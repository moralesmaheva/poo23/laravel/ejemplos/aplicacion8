@extends('layouts.main')

@section('content')
<div>
    <h1>Actualizar practica</h1>
</div>
    <form action="{{ route('practica.update', $practica) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        @include('practica._form')
    </form>
@endsection