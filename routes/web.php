<?php

use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\PerteneceController;
use App\Http\Controllers\PracticaController;
use App\Http\Controllers\PresentaController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
})->name('inicio');

//enrutamiento controlador alumno
Route::resource('alumno', AlumnoController::class);

//enrutammiento controlador curso
Route::resource('curso', CursoController::class);

//enrutamiento controlador practica
Route::resource('practica', PracticaController::class);

//enrutamiento controlador presentacion
Route::resource('presenta', PresentaController::class);


//enrutamiento controlador pertenece
Route::resource('pertenece', PerteneceController::class);

//enrutamiento adicional para practicas
Route::controller(PracticaController::class)->group(function () {
    Route::get('practica/confirmar/{practica}', 'confirmar')
        ->name('practica.confirmar');
});
